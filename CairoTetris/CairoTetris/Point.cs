﻿using System;

namespace CairoTetris
{
public class Point {
public int x,y;
public Point(int x,int y){
	this.x=x; this.y=y;
}
public Point(Point p) : this(p.x,p.y){
}
public Point clone(){
	return new Point(this);
}

public Point RotateCCW(){
	return new Point (-y,x);
}

public Point RotateCW(){
	return new Point (y,-x);
}

public Point Translate(Point p){
	return new Point (x + p.x, y + p.y);
}
}
}

