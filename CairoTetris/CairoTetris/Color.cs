﻿using System;

namespace CairoTetris
{
public class Color {
public double R,G,B;

public static Color White = new Color (1, 1, 1);
public static Color Black = new Color(0,0,0);

public Color(double r,double g,double b){
	this.R=r;
	this.G=g;
	this.B=b;
}

private static readonly Random random = new Random();

public Color(){
	this.R = random.NextDouble ();
	this.G = random.NextDouble ();
	this.B = random.NextDouble ();
}

public Color clone(){
	return new Color (this.R, this.G, this.B);
}
}
}

