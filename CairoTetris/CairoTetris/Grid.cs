﻿using System;
using System.Collections.Generic;

namespace CairoTetris
{
public class Grid
{
	private int width, height;
	public Color [,] bitmap;

public Grid (int width,int height)
{
	this.width = width;
	this.height = height;
	this.bitmap = new Color[width,height];
}




public bool Collisions(Shape shape){
	for (int i = 0; i < width; i++) {
		for (int j=0; j<height; j++) {
			if (bitmap[i,j]!=null && shape.Collisions (i,j)) {
				System.Console.WriteLine ("Shape collisions");
				return true;
			}
		}
	}

	return false;
}

public bool WithinBounds(Shape shape){
	return shape.WithinBounds (this.width, this.height);
}

public bool IsShapeValid(Shape shape){

	return !this.Collisions (shape) && this.WithinBounds (shape);
}






		public  void Rasterize(Shape shape){
			for (int i=0; i<shape.points.Count; i++) {
				Point p = shape.points [i];
				bitmap [p.x, p.y] = shape.color;
			}
		}
}
}

