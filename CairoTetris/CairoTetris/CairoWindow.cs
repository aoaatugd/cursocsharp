﻿using Gtk;
using Cairo;
using System;
using System.Collections.Generic;
using System.Threading;

namespace CairoTetris
{

class CairoWindow : Window {

public const int CELL_SIZE = 40;
public const int GRID_CELL_WIDTH = 10;
public const int GRID_CELL_HEIGHT = 22;
public const int GRID_POINT_WIDTH=CELL_SIZE*GRID_CELL_WIDTH;
public const int GRID_POINT_HEIGHT = CELL_SIZE*GRID_CELL_HEIGHT;
public Color gridColor= new Color(.5,.5,.5);
private readonly Grid grid;
public Color BackgroundColor = Color.Black;
DrawingArea darea;
System.Timers.Timer timer;
		Shape current;

public CairoWindow() : base("Basic shapes")
{
	this.timer = new System.Timers.Timer(500);
	timer.Elapsed+= HandleElapsed;
	this.timer.Enabled = true;

	grid = new Grid (GRID_CELL_WIDTH, GRID_CELL_HEIGHT);

	SetDefaultSize (GRID_POINT_WIDTH, GRID_POINT_HEIGHT);
	SetPosition (WindowPosition.Center);
	DeleteEvent += delegate {
		Application.Quit ();
	};

	KeyPressEvent += this.KeyPressEventHandler;

	darea = new DrawingArea ();
	darea.ExposeEvent += OnExpose;

	Add (darea);
	ShowAll ();
}

private Cairo.Context GetContext()
{
	Cairo.Context cc = Gdk.CairoHelper.Create (darea.GdkWindow);
	return cc;
}

private void DisposeContext(Cairo.Context cc)
{
	cc.GetTarget ().Dispose ();
	((IDisposable)cc).Dispose ();
}




		void HandleElapsed (object sender, System.Timers.ElapsedEventArgs e)
		{

			if (current == null) {
				generateShape ();
			} else {
				bool translated = this.Translate (0, 1);
				if (!translated) {
					generateShape ();
				}
			}
		}

		private bool RowIsFull(int y){
			for (int x=0; x<GRID_CELL_WIDTH; x++) {
				if (grid.bitmap [x, y] == null) {
					return false;
				}
			}
			return true;
		}

		private bool RowIsEmpty(int y){
			for (int x=0; x<GRID_CELL_WIDTH; x++) {
				if (grid.bitmap [x, y] != null) {
					return false;
				}
			}
			return true;
		}




		private void GarbageCollect()
		{
			bool foundFull = false;

			for (int y=0; y<GRID_CELL_HEIGHT; y++) {
				if (RowIsFull (y)) {
					foundFull = true;
					for (int x=0; x<GRID_CELL_WIDTH; x++) {
						grid.bitmap [x, y] = null;
					}
				}
			}


			if (foundFull) {

				for (int y=GRID_CELL_HEIGHT-1; y>0; y--) {
					while (RowIsEmpty (y) && !GridIsEmpty(y-1)) {
						for (int yy = y; yy>0; yy--) {
							ShiftRow (yy);
						}
					}
				}

				this.RedrawScene ();
			}
		}

		private bool GridIsEmpty(int starty){
			for (int y=starty; y>=0; y--) {
				if (!RowIsEmpty (y)) {
					return false;
				}
			}
			return true;
		}

		private void ShiftRow(int y){
			if (y > 0) {
				for (int x=0; x<GRID_CELL_WIDTH; x++) {
					grid.bitmap [x, y] = grid.bitmap [x, y - 1];
				}
				for (int x=0; x<GRID_CELL_WIDTH; x++) {
					grid.bitmap [0, y] = null;
				}
			}
		}

		private void RedrawScene(){
			Gtk.Application.Invoke (delegate {
				var cc = this.GetContext ();

				for (int x=0; x<GRID_CELL_WIDTH; x++) {
					for (int y=0; y<GRID_CELL_HEIGHT; y++) {
						this.DrawSquare (cc, x, y, grid.bitmap [x, y], grid.bitmap [x, y] != null);
					}
				}

				if (current != null) {
					this.DrawShape (cc, current, current.color, true);
				}

				DisposeContext (cc);
			});
		}

	

[GLib.ConnectBefore ()]
public void KeyPressEventHandler(object sender, KeyPressEventArgs args){

	switch (args.Event.Key) {
	case Gdk.Key.Up:
		this.Rotate (1);
		break;
	case Gdk.Key.Down:
		this.Rotate (-1);
		break;
	case Gdk.Key.Left:
		this.Translate (-1, 0);
		break;
	case Gdk.Key.Right:
		this.Translate (1, 0);
		break;
	case Gdk.Key.space:
		this.DropShape ();
		break;
	}
}

		public Random random = new Random();
		public ShapeType randomShapeType(){
			Array values = Enum.GetValues(typeof(ShapeType));
			return (ShapeType)values.GetValue(random.Next(values.Length));
		}

		public void generateShape(){
			bool generated = tryGenerateShape ();
			if (!generated) {
				// draw game over
				this.GameOver ();
				this.timer.Enabled = false;
			}
		}

		public bool tryGenerateShape()
		{
			bool generated = false;

			ShapeType shapeType = randomShapeType ();
			int x = GRID_CELL_WIDTH / 2 - 1;
    		int y = 0;

			Shape shape = new Shape (shapeType,x,y);

			Gtk.Application.Invoke (delegate {
				var cc = this.GetContext ();
				DrawShape (cc, shape, shape.color, true);
				DisposeContext (cc);
			});


			if (grid.IsShapeValid (shape)) {
				current = shape;
				System.Console.Out.WriteLine ("Generated shape");
				return true;
			} else {
				System.Console.Out.WriteLine ("Could not generate shape");
				return false;
			}
		}

	

public void GameOver(){
	Gtk.Application.Invoke (delegate {
		var cr = this.GetContext ();
		cr.SetSourceRGB (Color.White.R, Color.White.G, Color.White.B);

		cr.SelectFontFace ("Purisa", FontSlant.Normal, FontWeight.Bold);
		cr.SetFontSize (60);

		cr.MoveTo (GRID_POINT_WIDTH / 2 - 180, GRID_POINT_HEIGHT / 2);
		cr.ShowText ("Game Over");

		this.DisposeContext (cr);
			
		System.Console.Out.WriteLine("Game over");
	});
}

public void DropShape(){
	while (this.Translate (0, 1)) {
	}
}

public bool Rotate(int sign){
		Shape transformed = current.clone ();
		Shape original = current.clone ();
		transformed.Rotate (sign);
			if (grid.IsShapeValid (transformed)) {
				Gtk.Application.Invoke (delegate {
					this.Redraw (original, transformed);
				});
				current = transformed;
				return true;
			}

			return false;

}

public bool Translate(int x, int y){

			if (current == null)
				return false;

			Shape transformed = current.clone ();
			Shape original = current.clone ();
		    transformed.Translate (x, y);
			if (grid.IsShapeValid(transformed)) {
				Gtk.Application.Invoke (delegate {
					this.Redraw (original, transformed);
				});
				current = transformed;
				return true;
			} else {
				if (x == 0 && y == 1) {
					current = null;
					grid.Rasterize (original);

					// compute the lines that are full and remove
					this.GarbageCollect ();

				}
			}

			return false;

		}


public void Redraw(Shape original, Shape transformed){

	var cc = this.GetContext ();
	Redraw (cc, original, transformed);

	this.DisposeContext (cc);
}

public void Redraw(Cairo.Context cc, Shape original, Shape transformed){
			if (original != null) {
				DrawShape (cc, original, BackgroundColor, false);
			}
			if (transformed != null) {
				DrawShape (cc, transformed, transformed.color, true);
			}
}

public void DrawShape(Cairo.Context cc, Shape shape, Color color, bool drawGrid){
				for (int i = 0; i < shape.points.Count; i++) {
					DrawSquare (cc, shape.points [i].x, shape.points [i].y, color, drawGrid);
				}
}


public void DrawSquare(Cairo.Context cc,int cell_x, int cell_y, Color color, bool drawGrid)
{
			if (color == null) {
				color = BackgroundColor;
			}

			if (drawGrid) {
				cc.Rectangle (cell_x * CELL_SIZE, cell_y * CELL_SIZE, CELL_SIZE, CELL_SIZE);
				cc.SetSourceRGB (color.R, color.G, color.B);
				cc.FillPreserve ();
				cc.SetSourceRGB (gridColor.R, gridColor.G, gridColor.B);
				cc.Stroke ();
			} else {
				cc.Rectangle (cell_x * CELL_SIZE, cell_y * CELL_SIZE, CELL_SIZE, CELL_SIZE);
				cc.SetSourceRGB (color.R, color.G, color.B);
				cc.FillPreserve ();
				cc.Stroke ();
			}
}

void OnExpose(object sender, ExposeEventArgs args)
{
	var cc = this.GetContext ();
	cc.Rectangle (0, 0, GRID_POINT_WIDTH, GRID_POINT_HEIGHT);
	cc.SetSourceRGB (BackgroundColor.R,BackgroundColor.G,BackgroundColor.B);
	cc.Fill ();
	this.DisposeContext (cc);
}

}
}
