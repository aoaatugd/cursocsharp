﻿using System;
using Gtk;

namespace CairoTetris
{
class MainClass
{
public static void Main (string[] args)
{
	Application.Init ();
	Window win = new CairoWindow ();
	win.Show ();
	Application.Run ();
}
}
}
