﻿namespace CairoTetris {

	using System.Collections.Generic;

	public class Shape {


public ShapeType shapeType;
public List<Point> points;
public int centerIndex;
public Color color;

private Shape(){
	points = new List<Point> ();
}

public Shape clone(){
	Shape other = new Shape ();
			for (int i = 0; i < this.points.Count; i++) {
				other.points.Add (this.points [i].clone ());
			}
	other.color = this.color.clone ();
	other.centerIndex = this.centerIndex;
	other.shapeType = this.shapeType;
	return other;
}

public Shape (ShapeType shapeType, int x, int y) : this()
{
	this.color = new Color ();

	this.shapeType = shapeType;
			switch (shapeType) {
			case ShapeType.I:
				points.Add (new Point (x, y));
				points.Add (new Point (x, y + 1));
				points.Add (new Point (x, y + 2));
				points.Add (new Point (x, y + 3));
				centerIndex = 1;
				break;
			case ShapeType.J:
				points.Add (new Point (x + 1, y));
				points.Add (new Point (x + 1, y + 1));
				points.Add (new Point (x + 1, y + 2));
				points.Add (new Point (x, y + 2));
				centerIndex = 1;
				break;
			case ShapeType.L:
				points.Add (new Point (x, y));
				points.Add (new Point (x, y + 1));
				points.Add (new Point (x, y + 2));
				points.Add (new Point (x + 1, y + 2));
				centerIndex = 1;
				break;
			case ShapeType.S:
				points.Add (new Point (x + 1, y));
				points.Add (new Point (x + 2, y));
				points.Add (new Point (x, y + 1));
				points.Add (new Point (x + 1, y + 1));
				centerIndex = 0;
				break;
			case ShapeType.Z:
				points.Add (new Point (x, y));
				points.Add (new Point (x + 1, y));
				points.Add (new Point (x + 1, y + 1));
				points.Add (new Point (x + 2, y + 1));
				centerIndex = 1;
				break;
			case ShapeType.T:
				points.Add (new Point (x + 1, y));
				points.Add (new Point (x, y + 1));
				points.Add (new Point (x + 1, y + 1));
				points.Add (new Point (x + 2, y + 1));
				centerIndex = 2;
				break;
			case ShapeType.O:
				points.Add (new Point (x, y));
				points.Add (new Point (x, y + 1));
				points.Add (new Point (x + 1, y));
				points.Add (new Point (x + 1, y + 1));
				centerIndex = 0;
				break;

			}
}

public void Rotate(int sign){
	if (sign > 0) {
		this.RotateCW ();
	} else {
		this.RotateCCW ();
	}
}
		public bool CollisionsRow(int y){
			for (int i = 0; i < this.points.Count; i++) {
				if (this.points [i].y == y) {
					return true;
				}
			}

			return false;
		}

public bool WithinBounds(int x, int y)
		{
			for (int i = 0; i < this.points.Count; i++) {
				if (
					this.points [i].x < 0 || 
				    this.points [i].x >= x ||
					this.points [i].y < 0 ||
				    this.points [i].y >= y) {
					return false;
				}
			}

			return true;
		}

		public bool Collisions(int x,int y){
			for (int i = 0; i < this.points.Count; i++) {
				if (this.points [i].x == x && this.points [i].y == y) {
					return true;
				}
			}

			return false;
		}


public void RotateCW()
{
	if (this.shapeType == ShapeType.O) {
		return;
	}

	Point center = points [centerIndex].clone ();
	Translate (-center.x, -center.y);
	RotateOriginCW ();
	Translate (center.x, center.y);
}

public void RotateCCW(){
	if (this.shapeType == ShapeType.O) {
		return;
	}

	Point center = points [centerIndex].clone ();
	Translate (-center.x, -center.y);
	RotateOriginCCW ();
	Translate (center.x, center.y);
}

public void Translate(int x, int y){
	Translate (new Point (x, y));
}

public void Translate(Point p){
	for (int i = 0; i < this.points.Count; i++) {
		points[i]=points[i].Translate(p);
	}
}

public void RotateOriginCW(){
	for (int i = 0; i < this.points.Count; i++) {
		points [i] = points [i].RotateCW ();
	}
}

public void RotateOriginCCW(){
	for (int i = 0; i < this.points.Count; i++) {
		points [i] = points [i].RotateCCW ();
	}
}
}
}